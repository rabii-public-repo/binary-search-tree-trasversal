package bin.search.tree;

import java.util.LinkedList;
import java.util.Queue;

public class BinarySearchTree {
	Node root;
	int maxSize;

	public BinarySearchTree(int size) {
		this.maxSize = size;
	}

	void insert(int key) {
		Node newNode = new Node(key);
		if (root == null) {
			root = newNode;
			return;
		}
		Node current = root;
		Node parent = null;
		while (current != null) {
			parent = current;
			if (current.getKey() > key) {
				current = current.getLeft();
			} else {
				current = current.getRight();
			}
		}
		if (parent.getKey() > key) {
			parent.setLeft(newNode);
		} else {
			parent.setRight(newNode);
		}
	}

	public void inOrder(Node root) {
		if (root == null) {
			return;
		}
		inOrder(root.getLeft());
		System.out.print(root.getKey() + " ");
		inOrder(root.getRight());
	}

	public void preOrder(Node root) {
		if (root == null) {
			return;
		}
		System.out.print(root.getKey() + " ");
		preOrder(root.getLeft());
		preOrder(root.getRight());
	}

	public void postOrder(Node root) {
		if (root == null) {
			return;
		}
		
		postOrder(root.getLeft());
		postOrder(root.getRight());
		System.out.print(root.getKey()+" ");
	}

	public void levelOrder(Node root) {
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		while (!queue.isEmpty()) {
			Node node = queue.poll();
			System.out.print(node.getKey());
			if (node.getLeft() != null) {
				queue.add(node.getLeft());
			}
			if (node.getRight() != null) {
				queue.add(node.getRight());
			}
		}
	}
}
