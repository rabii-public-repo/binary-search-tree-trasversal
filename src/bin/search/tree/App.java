package bin.search.tree;

public class App {

	public static void main(String[] args) {
		BinarySearchTree bst = new BinarySearchTree(15);
		bst.insert(80549);
		bst.insert(235191);
		bst.insert(118837);
		bst.insert(-498143);
		bst.insert(-185405);
		bst.insert(-475495);
		bst.insert(428331);
		bst.insert(-146618);
		bst.insert(-479044);
		bst.insert(-173808);
		bst.insert(417544);
		bst.insert(204945);
		bst.insert(-479692);
		bst.insert(-70525);

		bst.preOrder(bst.root);
//		bst.inOrder(bst.root);
	}
}
